﻿<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; print ' '. $zebra; ?>">
	<table summary="评论内容">
		<tr>
			<td class="comment_author_name"><?php print t('!username', array('!username' => theme('username' , $comment))); ?></td>
			<td class="comment_date"><?php print t('!date', array('!date' => format_date($comment ->timestamp))); ?></td>
		</tr>
		<tr>
			<td class="comment_author_avatar"><?php if ($picture) { print $picture; } ?></td>
			<td class="comment_content_detail">
				<div class="content"><?php print $content; ?></div>
				<div class="links"><?php print $links; ?></div>
			</td>
		</tr>
	</table>
</div>
