<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
	<title><?php print $head_title ?></title>
	<?php print $head ?>
	<?php print $styles ?>
	<?php print $scripts ?>
</head>
<body>
<div id="header">
	<div class="header_left">
		<h1>
			<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>" onfocus="this.blur();">
				<?php if ($logo) { ?><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /><?php } ?><?php if ($site_name) { print $site_name;} ?>
			</a>
		</h1>
		<?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
	</div>
	<div class="header_right"><?php if($header) { ?><?php print $header ?><?php }; ?><?php if($search_box) { ?><?php print $search_box ?><?php }; ?>
	</div>
</div>
		<!-- #header end -->
<div id="main_menu">
		<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
		<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
</div>
		<!-- #main_menu end -->
<div id="main_wrapper">
	<?php if ($sidebar_left) { ?>
		<div id="sidebar_left">
			<?php print $sidebar_left ?>
			<div class="sidebar_left_footer"></div>	
		</div>
	<?php } ?>
	<div id="content_wrapper">
		<?php if ($breadcrumb): print $breadcrumb; endif; ?>
		<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
		<?php if ($content_top) { ?><div id="content_top"><?php print $content_top ?></div><?php } ?>
		<div id="main">
			<?php print $breadcrumd ?>
			<h2 class="title"><?php print $title ?></h2>
			<div class="tabs"><?php print $tabs ?></div>
			<?php print $help ?>
			<?php print $messages ?>
			<?php print $content; ?>
			<?php print $feed_icons; ?>
		</div>
		<?php if ($content_bottom) { ?><div id="content_bottom"><?php print $content_bottom ?></div><?php } ?>
		<div class="content_wrapper_footer"></div>
	</div>
</div>
		<!-- #main_wrapper end -->
<div id="footer_wrapper">
	<div id="footer">
		<?php print $footer_message ?>
		<span style="font-size: 11px;">theme <a href="http://www.Evance.name">BlackPark</a> by Evance</span><br />
  	</div>
</div>
		<!-- #footer end -->
<?php print $closure ?>
</body>
</html>